package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");
        final String object_id = getIntent().getExtras().getString("object_id");

        // INICIO - CODE6

        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e == null) {

                    TextView title = (TextView) findViewById(R.id.name);
                    title.setText((String) object.get("name"));

                    ImageView thumbnail = (ImageView) findViewById(R.id.thumbnail);
                    thumbnail.setImageBitmap((Bitmap) object.get("image"));

                    TextView price = (TextView) findViewById(R.id.price);
                    price.setText((String) object.get("price")+"\u0024");

                    TextView description = (TextView) findViewById(R.id.description);
                    description.setText((String) object.get("description"));
                    
                    description.setMovementMethod(new ScrollingMovementMethod());
                } else {

                }
            }
        });




        // FIN - CODE6

    }

}
